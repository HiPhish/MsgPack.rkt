#######################
 MessagePack in Racket
#######################

.. default-role:: code


This is a Racket_ implementation of MessagePack_, a binary data serialisation
format. It allows you to serialise (pack) and de-serialise (unpack) Racket
object to and from binary data.

.. _MessagePack: http://msgpack.org/
.. _Racket: http://racket-lang.org/


Installation
############

The easiest way to install this library is from the `Racket Package Catalog`_.
Run the following code from your shell:

.. code:: sh

   raco pkg install msgpack

If you wish to install the package from this repository use the included
makefile:

.. code:: sh

   make install   # Install the package
   make remove    # Uninstall the package
   make check     # Run unit tests

.. _Racket Package Catalog: https://pkgs.racket-lang.org/


Using MessagePack
#################

.. code:: racket

   ;; Import the library first
   (require msgpack)

   ;; Some object to pack
   (define hodgepodge (vector 1 2 (void) '#(3 #t) "foo"))

   ;; Packing data
   (define packed (pack hodgepodge out))
   ;> #"\225\1\2\300\222\3\303\243foo"

   ;; Unpacking data
   (define unpacked (unpack-from in))
   ;> '#(1 2 #<void> #(3 #t) "foo")

The `pack` procedure takes one or more Racket objects as arguments and returns
the serialised data. The `unpack` procedure takes a byte string and returns one
de-serialised object. We can also write bytes directly to a port and read
directly from a port.

.. code:: racket

   (pack-to (current-output-port) hodgepodge)

   (unpack-from (current-inrt))
   ;> '#(1 2 #<void> #(3 #t) "foo")

For more details please refer to the documentation_.

.. _documentation: https://docs.racket-lang.org/msgpack/index.html


Status
######

The library is fully functional, covered by test cases, and the API should be
reasonably mature, but I am not yet willing to completely rule out changes. See
also below for parts of the library that could not be tested at the moment due
to technical reasons.


Caveats
#######

The following cases cannot be tested for the time being:

- The `bin32` type, storing a byte string that is :math:`2^32` bytes long
  requires 4GiB, my machine simply runs out of memory.
- The same goes for the `str32` type
- The same goes for the `array32` type
- The same goes for the `map32` type
- The same goes for the `ext32` type
- Strings are only tested using ASCII characters, if anyone can generate
  UTF-8 strings with a given length in *bytes* please help out.


License
#######

Released under the GPLv3+ license, see the COPYING_ file for details.

.. _COPYING: COPYING.txt


Self-promotion
##############

If you like this library please consider financially supporting its
development, every small amount helps; you can become a regular supporter
through LiberaPay or tip me with a one-time donation via Ko-Fi. Feel free to
explore my other software projects as well.

http://hiphish.github.io/

.. image:: https://liberapay.com/assets/widgets/donate.svg
   :alt: Donate using Liberapay
   :target: https://liberapay.com/HiPhish/donate

.. image:: https://www.ko-fi.com/img/donate_sm.png
   :alt: Buy Me a Coffee at ko-fi.com
   :target: https://ko-fi.com/F1F7LPG1
